# Neurological disorders #

*Author*: Joanna Cichowska

*Description*: The project aims to simulate the way neurological malfunctions impact human ability to see, move and react to the environment. Conditions implemented in the present version (quite initial one, in fact) are: shortsightedness ('myopia'), farsightedness ('hyperopia'), cerebellum disorders (denoted as 'cerebellum'; a generalization of cerebellar symptoms), one- or two-sided blindness ('left eye disabled'/'right eye disabled' or both, respectively) and lameness ('left side disabled'/'right side disabled'; a generalization again). The user can add humans with chosen characteristics and obstacles to the randomly generated environment and observe how they behave. To be continued.

*Instruction*: Download and unzip files. In Eclipse, create new project. Set its location to the folder where the files are. Open Simulation.java and run.

*Additional information*:

* In the 'Options' window, click on a chosen condition to trigger it. When you choose 'Myopia' or 'Hyperopia', you can set intensity of it (approximately in diopters) using the slide. Notice than you cannot choose 'Myopia' AND 'Hyperopia' at the same time - in order to change your choice once you have clicked on one of them, you need to unclick the current option.

* You can also choose a type of objects to add (if nothing is chosen, random shapes will be generated). The same restriction (no more than one type chosen) applies here.

* Once you have chosen, left-click anywhere you wish inside the second window to insert a person or right-click to add an object.

* You can left-click on an existing person to reset its position to the middle of the window. Trying to gather everyone in the center is surprisingly addictive.

* Press P to pause the simulation.

* Press UP/DOWN arrows to increase/decrease the simulation's speed.

* Press Esc to exit.

* When a person hits a car, he or she dies. And disappears.